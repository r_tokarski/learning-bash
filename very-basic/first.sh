#!/usr/bin/env bash

# default setting, overwritten if launched with an argument
DEFAULT_COLOR="red"
SHIRT_COLOR="$1"
MY_SHIRT_COLOR="My shirt color is ${SHIRT_COLOR:-$DEFAULT_COLOR}"
echo $MY_SHIRT_COLOR

