#!/usr/bin/env bash

mytable=(uno dos tres cuarto cinco seis)
mytable_base=(${mytable[*]})
echo ${mytable_base[*]}
echo ${mytable[0]} ${mytable[2]} ${mytable[4]}
unset mytable[0] mytable[2] mytable[4]
echo "${#mytable[*]} items were not shown, out of $(echo ${#mytable_base[*]})."
