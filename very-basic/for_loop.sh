#!/bin/bash

dellist=( gnome-connections gnome-contacts gnome-boxes gnome-tour gnome-photos gnome-maps \
  totem rhythmbox simple-scan iwl100-firmware iwl1000-firmware iwl105-firmware iwl135-firmware \
  iwl2000-firmware iwl2030-firmware iwl3160-firmware iwl3945-firmware iwl4965-firmware \
  iwl5000-firmware iwl5150-firmware iwl6000-firmware iwl6000g2a-firmware iwl6000g2b-firmware \
  iwl6050-firmware iwlax2xx-firmware )

for appid in "${dellist[@]}"; do
  if rpm -qi "$appid" > /dev/null; then
    continue
  else
    dellist=( "${dellist[@]/$appid}")
    # echo "${dellist[@]}"
  fi
done

echo "dnf remove ${dellist[*]}"
# if "${#dellist[*]}" -eq  0 ; then
#   echo "Nothing to uninstall."
# else
#   echo "${dellist[@]}"
# fi
