#!/bin/env bash
if [[ $1 = 1 ]]; then
  cat /etc/hostname && cat /etc/fstab # echo !? = 0
elif [[ $1 = 2 ]]; then
  cat /etc/redhat-release || cat /etc/debian_version # first is false. echo !? = 0
else
  cat && cat /etc/mtab # echo !? = 1
fi
