#!/bin/bash

# This is a simple script to restore latest backup using borgbackup
# It is intended to use after fedora-postinstall.sh

# vars
borg_dir=/media/backup/borg/castleyankee

if $(hostnamectl chassis) -eq desktop; then
  if ! "$(tree "$BORG_KEYS_DIR" | grep -q castleyankee)"; then
    cd /
    sudo -u "$SUDO_USER" borg key import /media/backup/borg-castleyankee.key
    sudo -u "$SUDO_USER" borg extract "$borg_dir"::"$(borg list "$(borg_dir)" --format '{archive}{NL}' | tail -n1)"
  fi
fi
