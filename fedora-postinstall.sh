#!/bin/bash

# This is my try to write a bash script to automate my post-install process on Fedora. 
# I have two Fedora devices (CastleYankee and CastleEcho), so I write it for both.
# This is heavily based on my 'fedora_reinstall.yml' ansible playbook,
# but written in bash and for local use only.

# variables
domain=hexezin.home
addlist=( iwl7260-firmware zsh bash-completion tree yt-dlp yt-dlp-zsh-completion hexchat deluge borgbackup \
  codium brave-browser podman toolbox libreoffice-langpack-pl hunspell-es hunspell-pl wget aria2 \
  speedtest-cli neofetch vim-enhanced neovim gnome-mines gnome-weather gnome-shell-extension-gsconnect \
  gnome-shell-extension-openweather gnome-shell-extension-user-theme gnome-pomodoro gnome-tweaks \
  piper evolution corectrl yubikey-personalization-gui mpv gimp eog virt-manager vagrant vagrant-libvirt \
  ripgrep ncdu doa nodejs )
dellist=( gnome-connections gnome-contacts gnome-boxes gnome-tour gnome-photos gnome-maps \
  totem rhythmbox simple-scan iwl100-firmware iwl1000-firmware iwl105-firmware iwl135-firmware \
  iwl2000-firmware iwl2030-firmware iwl3160-firmware iwl3945-firmware iwl4965-firmware \
  iwl5000-firmware iwl5150-firmware iwl6000-firmware iwl6000g2a-firmware iwl6000g2b-firmware \
  iwl6050-firmware iwlax2xx-firmware )
fstabl=""
f=0

# check for Fedora
if ! grep -q ID=fedora /etc/os-release; then
  printf "You are not using Fedora, aborting.\n"
  exit 1
fi

# Set hostname
printf "Setting up hostname...\n"
if [ "$(hostnamectl chassis)" = laptop ]; then
  if [ "$(hostnamectl hostname)" = castleecho.$domain ]; then
   printf "Hostname CastleEcho already set, nothing to do.\n"
  else
    hostnamectl hostname castleecho.$domain
    hostnamectl chassis hostname --pretty CastleEcho
   printf "Hostname set to CastleEcho.\n"
  fi
elif [ "$(hostnamectl chassis)" = desktop ]; then
  if [ "$(hostnamectl hostname)" = castleyankee.$domain ]; then
   printf "Hostname CastleYankee already set, nothing to do.\n"
  else
    hostnamectl hostname castleyankee.$domain
    hostnamectl hostname --pretty CastleYankee
   printf "Hostname set to CastleYankee.\n"
  fi
elif [ "$(hostnamectl chassis)" = vm ]; then
  if [ "$(hostnamectl hostname)" = castleunion.$domain ]; then
   printf "Hostname CastleUnion already set, nothing to do. Continue learning bash.\n"
  else
    hostnamectl hostname castleunion.$domain
    hostnamectl hostname --pretty CastleUnion
   printf "Hostname set to CastleUnion. Let's learn some bash.\n"
  fi
else
 printf "Unknown device, aborting.\n"
  exit 2
fi

# add devices to /etc/hosts
# more work to do here - if device is found in /etc/hosts,
# print that line and let decide what to do before skipping
printf "Adding devices to /etc/hosts...\n"
if grep -Fq "hualcoyotl" /etc/hosts; then
 printf "hualcoyotl already in /etc/hosts - nothing to do.\n"
else
  printf "\n############### added using %s ###############\n192.168.1.69 hualcoyotl hualcoyotl.%s\n" "${0##*/}" \
    "$domain" | tee -a /etc/hosts > /dev/null
fi
if [ "$(hostnamectl chassis)" = desktop ]; then
  if grep -Fq "castleecho" /etc/hosts; then
   printf "CastleEcho already in /etc/hosts - nothing to do.\n"
  else
   printf "10.21.32.4 castleecho castleecho.%s" "$domain" | tee -a /etc/hosts > /dev/null
  fi
elif [ "$(hostnamectl chassis)" = laptop ]; then
  if grep -Fq "castleyankee" /etc/hosts; then
   printf "CastleYankee already in /etc/hosts - nothing to do.\n"
  else
   printf "10.21.32.3 castleyankee castleyankee.%s" "$domain" | tee -a /etc/hosts > /dev/null
  fi
elif [ "$(hostnamectl chassis)" = vm ]; then
  if grep -Fq castleyankee /etc/hosts; then
   printf "CastleYankee and CastleEcho already in /etc/hosts - nothing to do.\n"
  else
    printf "10.21.32.3 castleyankee castleyankee.%s\n10.21.32.4 castleecho castleecho.%s\n" \
     "$domain" "$domain" | tee -a /etc/hosts > /dev/null
  fi
fi

# configure DNF
if [ ! -f /etc/dnf/dnf.conf."$(date +%F)" ]; then
  mv /etc/dnf/dnf.conf /etc/dnf/dnf.conf."$(date +%F)"
  printf "Backup was created for /etc/dnf/dnf.conf: /etc/dnf/dnf.conf.%s\n" "$(date +%F)"
fi
printf "[main]\ngpgcheck=1\ninstallonly_limit=2\nclean_requirements_on_remove=True\nbest=False\n\
skip_if_unavailable=True\nmax_parallel_downloads=20\nfastestmirror=1\n" | tee /etc/dnf/dnf.conf > /dev/null

# add repositories for Brave Browser, SCodium and RPM-Fusion
# instructions are almost 100% copy-pasted from
# https://brave.com and from https://vscodium.com
printf "Adding Brave Browser, VSCodium and RPM-Fusion repositories...\n"
if test -f /etc/yum.repos.d/brave-browser; then
 printf "Brave Browser repository is already added, nothing to do.\n"
else
  printf "[BraveBrowser]\nname=Brave Browser\nbaseurl=https://brave-browser-rpm-release.s3.brave.com/x86_64/\nenabled=1" \
    | tee /etc/yum.repos.d/brave-browser.repo > /dev/null
  rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
fi
if test -f /etc/yum.repos.d/vscodium.repo; then
 printf "VSCodim repository is already added, nothing to do.\n"
else
  rpmkeys --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg
  printf "[VSCodium]\nname=VSCodium\nbaseurl=https://download.vscodium.com/rpms/\nenabled=1\ngpgcheck=1\n\
repo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg\nmetadata_expire=1h" \
    | tee /etc/yum.repos.d/vscodium.repo > /dev/null
fi

if rpm -qi "rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm" > /dev/null; then
  printf "RPM-Fusion repos are already added, nothing to do.\n"
else
  dnf -qy install "https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm" \
    "https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm"
fi

# update repositories
dnf -q makecache

# install software that i use
for appid in "${addlist[@]}"; do
  if rpm --quiet -qi "$appid" > /dev/null; then
    addlist=( "${addlist[@]/$appid}")
  else
    continue
  fi
done
if [ "${#addlist[@]}" -eq 0 ]; then
 printf "Everything desired already installed.\n"
else
  dnf -qy install "${addlist[@]}" 
fi

# remove unneeded default software
for appid in "${dellist[@]}"; do
  if rpm --quiet -qi "$appid"; then
    continue
  else
    dellist=( "${dellist[@]/$appid}" )
  fi
done
if [ "${#dellist[@]}" -eq 0 ]; then
 printf "Nothing to uninstall.\n"
else
  dnf -qy remove "${dellist[@]}"
fi

# update system
dnf -qy upgrade

# clone my dotfiles repo and sync
if [ ! -d ~/Plantillas/git/ ]; then
  mkdir ~/Plantillas/git
  sudo -u "$SUDO_USER" git clone https://gitlab.com/r_tokarski/dotfiles /home/"$SUDO_USER"/Plantillas/git > /dev/null
  sudo -u "$SUDO_USER" stow ~/Plantillas/git/dotfiles/zsh ~/
  sudo -u "$SUDO_USER" stow ~/Plantillas/git/dotfiles/kitty ~/
  sudo -u "$SUDO_USER" stow ~/Plantillas/git/dotfiles/mpv ~/
fi

# install Astronvim
if [ ! -d ~/.config/nvim/.git ] && [ -d ~/.config/nvim ]; then
  sudo -u "$SUDO_USER" mv ~/.config/nvim ~/.config/nvim-"$(date +%F)"
else
  printf "Installing Astronvim...\n"
  sudo -u "$SUDO_USER" git clone https://github.com/AstroNvim/AstroNvim ~/.config/nvim > /dev/null
  sudo -u "$SUDO_USER" stow /home/"$SUDO_USER"/Plantillas/git/dotfiles/nvim-user ~/
fi

# create mount points 
 for m in descargas backup ventoy nfs; do
  if ! test -d /media/$m; then
    printf "Adding mountpoints and devices to /etc/fstab...\n"
    mkdir /media/$m
  fi
done

# mount filesystems
fstabl=$(wc -l /etc/fstab | cut -f1 -d' ')

if [ "$(hostnamectl chassis)" = desktop ] && ! grep -Fq UUID=9836683C36681D8E /etc/fstab; then
  printf "UUID=9836683C36681D8E    /media/descargas    ntfs    defaults,noauto 0 0\n" | tee -a /etc/fstab > /dev/null
  f=$((f+1))
fi
if [ "$(hostnamectl chassis)" = desktop ] && ! grep -Fq UUID=5aa5a70d-92fd-431d-9bef-dd229128ab4c /etc/fstab; then
  printf "UUID=5aa5a70d-92fd-431d-9bef-dd229128ab4c    /media/backup   xfs   defaults,noauto 0 0\n" | tee -a /etc/fstab > /dev/null
  f=$((f+1))
fi
if [ "$(hostnamectl chassis)" != vm ] && ! grep -Fq UUID=4492-E896 /etc/fstab; then
  printf "UUID=4492-E896   /media/ventoy   exfat   defaults,noauto,uid=1000,gid=1000 0 0\n" | tee -a /etc/fstab > /dev/null
  f=$((f+1))
fi
if ! grep -Fq hualcoyotl /etc/fstab; then
  printf "hualcoyotl:/   /media/nfs    nfs   defaults,timeo=900,retrans=5,_netdev,user 0 0\n" | tee -a /etc/fstab > /dev/null
  f=$((f+1))
fi
if [ $f -gt 0 ]; then
  sed -i "$((fstabl+1))i $(printf '############### added using %s ###############\n' "${0##*/}")" | tee -a /etc/fstab > /dev/null
fi
mount -a

# change default shell to zsh
if [ "$SHELL" = /bin/bash ]; then
  usermod -s /bin/zsh "$SUDO_USER"
  printf "Default shell changed to zsh.\n"
fi

# create brave-wayland.desktop file
if [ ! -f /home/"$SUDO_USER"/.local/share/applications/brave-wayland.desktop ]; then
  sudo -u "$SUDO_USER" cp /usr/share/applications/brave-browser.desktop \
    /home/"$SUDO_USER"/.local/share/applications/brave-wayland.desktop
  sudo -u "$SUDO_USER" sed -i "s/%U/--enable-features=UseOzonePlatform --ozone-platform=wayland %U/" \
    /home/"$SUDO_USER"/.local/share/applications/brave-wayland.desktop
  printf "brave-wayland.desktop was created.\n"
fi

